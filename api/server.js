const express = require('express');
const CONFIG = require('./config.js');
const app = express();
const axios = require('axios');
const md5 = require('md5');

app.use('/', (req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Content-Type', 'text/javascript');
  next();

})

app.get('/api/getMarvelCharacters', function (req, res) {

  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Content-Type', 'text/javascript');

  axios.get(`https://gateway.marvel.com:443/v1/public/characters?${getAuthString()}&offset=${req.query.offset}&limit=${req.query.limit}`)
    .then((result) => {
      res.send(result.data.data);
    })
    .catch(function (error) {
      // handle error
      console.error(error);
    })
});


app.listen(5000, function () {
  console.log('Example app listening on port 5000!')
});


function getAuthString() {
  const ts = (new Date()).getTime();
  const hash = md5(ts + CONFIG.MARVEL_API_PRIVATE_KEY + CONFIG.MARVEL_API_PUBLIC_KEY);

  return `ts=${ts}&hash=${hash}&apikey=${CONFIG.MARVEL_API_PUBLIC_KEY}`;
}