module.exports = {
  MARVEL_API_URL: 'https://gateway.marvel.com:443/v1/public',
  MARVEL_API_PUBLIC_KEY: 'cc323baac9cf9414f58f0643ee25caf9',
  MARVEL_API_PRIVATE_KEY: process.env.MARVEL_API_PRIVATE_KEY
};