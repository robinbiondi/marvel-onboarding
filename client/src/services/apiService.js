import CONFIG from '../config';
export function fetchCharacters(offset, limit) {
  return fetch(`${CONFIG.API_URL}/api/getMarvelCharacters?offset=${offset}&limit=${limit}`)
    .then(res => res.json());
}