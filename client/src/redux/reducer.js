import {FETCHED_CHARACTERS, FETCHING_CHARACTERS,HIGHLIGHT_CHARACTER, UNLIGHT_CHARACTER} from './actions'
const defaultState = {
  loading: false,
  characters: [],
  nbCharacters: 0,
  highlight: null,
}
export default function reducer(state = defaultState, action) {
  switch(action.type) {
    case FETCHED_CHARACTERS:
      return Object.assign({}, state, {
        loading: false,
        characters: [
          ...state.characters,
          ...action.characters,
        ],
        nbCharacters: action.total,
      });
    case FETCHING_CHARACTERS:
      return Object.assign({}, state, {
        loading: true,
      });
    case HIGHLIGHT_CHARACTER:
      return Object.assign({}, state, {
        highlight: action.character,
      });
      case UNLIGHT_CHARACTER:
        return Object.assign({}, state, {
          highlight: null,
        });
    default:
      return state;
  }
}