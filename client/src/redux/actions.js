export const FETCHED_CHARACTERS = 'FETCHED_CHARACTERS';

export function fetchedCharacters(characters, total) {
  return {
    type: FETCHED_CHARACTERS,
    characters,
    total
  };
}

export const FETCHING_CHARACTERS = 'FETCHING_CHARACTERS';

export function fetchingCharacters() {
  return {
    type: FETCHING_CHARACTERS,
  };
}

export const HIGHLIGHT_CHARACTER = 'HIGHLIGHT_CHARACTER';

export function highlightCharacter(character) {
  return {
    type: HIGHLIGHT_CHARACTER,
    character,
  };
}

export const UNLIGHT_CHARACTER = 'UNLIGHT_CHARACTER';

export function unlightCharacter() {
  return {
    type: UNLIGHT_CHARACTER,
  };
}
