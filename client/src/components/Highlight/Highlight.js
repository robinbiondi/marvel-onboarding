import React, { Component } from 'react';
import {connect} from 'react-redux';
import './Highlight.css';
import {unlightCharacter} from '../../redux/actions';


const  mapStateToProps = (state) => ({
  highlight: state.highlight,
});

const  mapDispatchToProps = (dispatch) => ({
  unlightCharacter: () => dispatch(unlightCharacter()),
});

class Highlight extends Component {

  render() {
    const {highlight} = this.props;
    const bannerStyle = highlight
      ? {
        backgroundImage: `url(${highlight.thumbnail.path}/landscape_incredible.jpg)`
      }
      : null;

    const classes = [
      'App-highlight',
      highlight ? 'App-highlight--visible' : null,
    ].join(' ');


    return (
      <div className={classes}>
        <span className="App-highlight__close" onClick={this.handleClose}>close</span>
        <div className="App-highlight__banner" style={bannerStyle}></div>
        <div className="App-highlight__name"><h1>{highlight ? highlight.name :''}</h1></div>
      </div>
    );
  }

  renderText = () => {
    const {isLoading,nbCharacters} = this.props;

    if(isLoading)
      return 'Wait a seconds newbie, heroes are coming...';
    return `${nbCharacters} characters ... It is a lot of faces to remember...`
  }

  handleClose = () => {
    const {unlightCharacter} = this.props;
    return unlightCharacter();
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Highlight);