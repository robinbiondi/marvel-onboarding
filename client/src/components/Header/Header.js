import React, { Component } from 'react';
import {connect} from 'react-redux';
import logo from '../../assets/logo.svg';
import './Header.css';

const  mapStateToProps = (state) => ({
  nbCharacters: state.nbCharacters,
  characters: state.characters,
  nbCharactersLoaded: state.characters.length,
  isLoading: state.loading,
});


class Header extends Component {

  render() {

    return (
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>{this.renderText()}</p>
      </header>
    );
  }

  renderText = () => {
    const {isLoading,nbCharacters} = this.props;

    if(isLoading)
      return 'Wait a seconds newbie, heroes are coming...';
    return `${nbCharacters} characters ... It is a lot of faces to remember...`
  }

}

export default connect(mapStateToProps)(Header);