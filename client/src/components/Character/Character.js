import React, { Component } from 'react';
import {connect} from 'react-redux';
import {highlightCharacter} from '../../redux/actions';
import './Character.css';

const  mapStateToProps = (state) => ({});

const  mapDispatchToProps = (dispatch) => ({
  highlightCharacter: (character) => dispatch(highlightCharacter(character)),
});

class Character extends Component {

  render() {
    const {character} = this.props;
    const sectionStyle = {
      backgroundImage: `url(${character.thumbnail.path}/standard_xlarge.jpg)`
    };

    return (
      <div className="App-character" onClick={this.handleClick} style={sectionStyle}>
        <div className="App-character__name">{character.name}</div>
      </div>
    )
  }

  handleClick = () => {
    const { character, highlightCharacter } = this.props;

    return highlightCharacter(character);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Character);
