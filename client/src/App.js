import React, { Component } from 'react';
import {connect} from 'react-redux';
import {fetchCharacters} from './services/apiService';
import {fetchedCharacters, fetchingCharacters} from './redux/actions';
import logo from './assets/logo.svg';
import './App.css';
import Character from './components/Character/Character';
import Header from './components/Header/Header';
import Highlight from './components/Highlight/Highlight';

const  mapStateToProps = (state) => ({
  nbCharacters: state.nbCharacters,
  characters: state.characters,
  nbCharactersLoaded: state.characters.length,
  isLoading: state.loading,
});

const  mapDispatchToProps = (dispatch) => ({
  fetchedCharacters: (characters, total) => dispatch(fetchedCharacters(characters, total)),
  fetchingCharacters: () => dispatch(fetchingCharacters()),
});

class App extends Component {
  componentWillMount() {
    this.fetchCharacters();
  }

  render() {
    return (
      <div className="App">
        <Header />
        <div className="App-body" onScroll={this.handleScroll}>
          {this.renderCharacters()}
          {this.renderLoader()}
        </div>
        <Highlight />
      </div>
    );
  }

  renderCharacters = () => {
    const {characters} = this.props;

    return characters.map((character) => (<Character key={character.id} character={character}/>))
  }

  renderLoader = () => {
    const {isLoading} = this.props;

    if (!isLoading)
      return null;
    return (
      <div className="App-loader">
        <img src={logo} className="App-loader__icon" alt="loader" />
      </div>
    );
  }

  handleScroll = (e) => {
    const { nbCharacters, nbCharactersLoaded} = this.props;
    const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;

    if (bottom && nbCharactersLoaded < nbCharacters) {
      return this.fetchCharacters()
    }

    return Promise.resolve();
  }

  fetchCharacters = () => {
    const {fetchedCharacters, fetchingCharacters, nbCharactersLoaded, isLoading} = this.props;

    if(isLoading)
      return Promise.resolve();
    fetchingCharacters();
    return fetchCharacters(nbCharactersLoaded, 25)
      .then((result) => {
        fetchedCharacters(result.results, result.total)
      });
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
