# marvel-onboarding


## How to start the app ?
In order to start the app, just use `MARVEL_API_PRIVATE_KEY={your-marvel-api-private-key} docker-compose up`
You can access the site at [http://localhost:3000](http://localhost:3000)
You are in development mode and the code you edit is reloaded dinamically

## Where can i see the release of the app ?

You can find the release of the app at this [http://ec2-54-194-35-99.eu-west-1.compute.amazonaws.com/](http://ec2-54-194-35-99.eu-west-1.compute.amazonaws.com/)

## How is working the solution ?

The solution is composed of two parts:
- The client: a react app using redux
- the api: a node server which is used to make authorized calls to marvel api

Those two projects are run in two several docker containers.
In production, the two containers are deployed on the same EC2 using ECS from AWS.

As so, releases are easy and it will be easier to setup a CI/CD solution.


## What I would do if I had more time ?

- Test each front component with `react-test-render` and jest
- Test the redux part entirely
- Setup flow in order to type the front end
- setup a coherent linter and clean the code
- create a deployment script avoiding me to deploy with ECS console
- Add a button that would allow to load more content on big screen where there is no need to scroll